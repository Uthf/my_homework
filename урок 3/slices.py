lst = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
print(lst[2:8:2])
print(lst[5:9])
print(lst[:5:2])
print(lst[1:])
print(lst[::-1])

lst[2:8:2] = 3, 1, 9
print(lst)