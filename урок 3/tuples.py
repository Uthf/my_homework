tpl = ()
tpl2 = ()
tpl3 = (1,2.5, True, None, "sting")
tpl4 = (6,)
tpl5 = 1,2.5, True, None, "sting"
tpl6 = 6,
tpl7 = tuple('hello')
a, b = 6, 8

T1 = (1, 2, 3)
L1 = [1, 2, 3]
T2 = T1
L2 = L1
T2 += (4, 5)
L2 += [4, 5]
print(T1, L1)
print(T2, L2)