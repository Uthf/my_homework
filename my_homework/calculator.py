import os
# этот блок определяет функции, которые можно сделать с числами

def addition(a, b):
    return print("Ответ: {} + {} =".format(a, b), a + b)


def subtraction(a, b):
    return print("Ответ: {} - {} =".format(a, b), a - b)


def multiplication(a, b):
    return print("Ответ: {} * {} =".format(a, b), a * b)


def division(a, b):
    return print("Ответ: {} / {} =".format(a, b), a / b)


def exponentation(a, b):
    return print("Ответ: {} ** {}".format(a, b), a ** b)


def cls():
    os.system('cls' if os.name=='nt' else clear)


print('''Привет! Я калькулятор. Я умею выполнять базовые математические функции с двумя числами.
Для того чтобы воспользоваться вам нужно ввесте первое число, операцию, которую вы хотите произвести, и второе число. 
''')

first_number = int(input("Введите первое число:\n" ))
cls()

# этот блок вводит два чила и операцию с ними
print('''Для выбора доступны следующие операции:
Для сложения введите - 1
Для вычитания введите - 2
Для умножения введите - 3
Для деления введите - 4
Для возведения первого числа в степень второго введите - 5
''')

operation = int(input("Введи операцию:\n"))
cls()
second_number = int(input("Введите второе число:\n"))
cls()

# этот блок выводит решение
if operation == 1:
    solution = addition(first_number, second_number)
elif operation == 2:
    solution = subtraction(first_number, second_number)
elif operation == 3:
    solution = multiplication(first_number, second_number)
elif operation == 4:
    if second_number == 0:
        print("На ноль делить нельзя")
        second_number = int(input("Введите второе число:\n"))
        cls()
    solution = division(first_number,second_number)
elif operation == 5:
    solution = exponentation(first_number, second_number)
else:
    print("Попробуй начать сначала.")
    