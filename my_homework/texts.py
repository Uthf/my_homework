'''
Программа считывает 3 строки
Затем программа выводит:
1 число - кол-во уникальных символов в 1 строке
2 число - кол-во уникальных символов во 2 строке
3 число - кол-во уникальных символов во 3 строке
4 число - кол-во общих уникальных символов среди 3х строк
5 число - кол-во уникальных символов среди 3х строк 
'''
a = set(input())
b = set(input())
c = set(input())

print(len(a))
print(len(b))
print(len(c))
print(len(a & b & c))
print(len(a | b | c))
