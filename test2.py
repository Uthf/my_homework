

from sqlalchemy import create_engine, Column, DateTime, func, Integer, String, ForeignKey, Float, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import join
from sqlalchemy.orm import sessionmaker
import datetime




Base = declarative_base() 


class Item(Base):
    __tablename__ = "items"

    item_id = Column('id', Integer, primary_key=True)
    item_name = Column('name', String(30), unique=True, nullable=False)
    item_price = Column('price', Float, nullable=False)


class Procurement(Base):
    __tablename__ = "procurements"

    procurement_id = Column(Integer, primary_key=True)
    item_id = Column(Integer, ForeignKey(f"{Item.__tablename__}.id"))
    procurement_quantity = Column(Integer, nullable=False)
    procurement_date = Column(Date, nullable=False)


class Sale(Base):
    __tablename__ = "sales"

    sale_id = Column(Integer, primary_key=True)
    item_id = Column(Integer, ForeignKey(f"{Item.__tablename__}.id"))
    bartender_name = Column(String(30))
    sale_quantity = Column(Integer, nullable=False)
    sale_date = Column(Date, nullable=False)


class Role:
    Session = sessionmaker()

    def __init__(self):
        self.engine = create_engine("sqlite:///test2.sqlite3", echo=False)
        self.session = self.Session(bind=self.engine)
        Base.metadata.create_all(self.engine)

class Bartender(Role):
    def sale(self, name, bname, quantity):
        item = self.session.query(Item).filter(Item.item_name == name).first()
        sale = Sale(
            item_id=item.item_id, 
            bartender_name=bname, 
            sale_quantity=quantity, 
            sale_date=datetime.datetime.now(),
            )

        self.session.add(sale)
        self.session.commit()


class Admin(Bartender):
    def procure(self, name, quantity):
        item = self.session.query(Item).filter(Item.item_name == name).first()
        procure = Procurement(
            item_id=item.item_id, 
            procurement_quantity=quantity, 
            procurement_date=datetime.datetime.now(),
            )
        self.session.add(procure)
        self.session.commit()

    def add_product(self, name, price):
        self.session.add(Item(item_name=name, item_price=price))
        self.session.commit()
    
admin = Admin()
bartender = Bartender()

admin.add_product(name="Водка", price=150.50)
admin.add_product(name="Пиво", price=50.20)
admin.add_product(name="Виски", price=520.30)
admin.add_product(name="Чипсы", price=30.40)

admin.procure(name="Водка", quantity=70)
admin.procure(name="Пиво", quantity=150)
admin.procure(name="Виски", quantity=30)
admin.procure(name="Чипсы", quantity=250)

bartender.sale(name="Водка", bname='Alex', quantity=3)
bartender.sale(name="Виски", bname='Alex', quantity=1)
bartender.sale(name="Чипсы", bname='Ivan', quantity=10)
bartender.sale(name="Пиво", bname='Ivan', quantity=5)
