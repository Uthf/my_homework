

import tkinter as tk
import requests

HEIGHT = 300
WIDTH = 500

def test_function(entry):
	print("This is the entry:", entry)

def format_response(weather):
	try:
		name = weather['name']
		temp = round(weather['main']['temp'] - 273)
		wind_speed = weather['wind']['speed']
		wind_deg = weather['wind']['deg']
		if 337.5 < wind_deg <= 22.5:
			wind_direction = '⬆'
		elif 22.5 < wind_deg <= 67.5:
			wind_direction = '↗'
		elif 67.5 < wind_deg <= 112.5:
			wind_direction = '➡'
		elif 112.5 < wind_deg <= 157.5:
			wind_direction = '↘'
		elif 157.5 < wind_deg <= 202.5:
			wind_direction = '⬇'
		elif 202.5 < wind_deg <= 247.5:
			wind_direction = '↙'
		elif 247.5 < wind_deg <= 292.5:
			wind_direction = '⬅'	
		elif 292.5 < wind_deg <= 337.5:
			wind_direction = '↖'					
		final_str = f'Город: {name} \nТемпература: {temp}°С'
		final_str += f'\nВетер: {wind_speed} м/с, {wind_direction}'
	except:
		final_str = 'Уточните ваш город'

	return final_str

def get_weather(city):
	appid = 'd73ae7ff2f0f79c177156790453b0cb1'
	url = 'https://api.openweathermap.org/data/2.5/weather'
	params = {'APPID': appid, 'q': city,}
	response = requests.get(url, params=params)
	weather = response.json()

	label['text'] = format_response(weather)


root = tk.Tk()

canvas = tk.Canvas(root, height=HEIGHT, width=WIDTH)
canvas.pack()

background_image = tk.PhotoImage(file='landscape.png')
background_label = tk.Label(root, image=background_image)
background_label.place(relwidth=1, relheight=1)

frame = tk.Frame(root, bg='#34ebc0', bd=5)
frame.place(relx=0.5, rely=0.1, relwidth=0.75, relheight=0.1, anchor='n')

entry = tk.Entry(frame, font=38)
entry.place(relwidth=0.65, relheight=1)

button = tk.Button(frame, text="Узнать погоду", font=38, command=lambda: get_weather(entry.get()))
button.place(relx=0.7, relheight=1, relwidth=0.3)

lower_frame = tk.Frame(root, bg='#34ebc0', bd=10)
lower_frame.place(relx=0.5, rely=0.25, relwidth=0.75, relheight=0.6, anchor='n')

label = tk.Label(lower_frame, bg='#e4ede4', font=50)
label.place(relwidth=1, relheight=1)

root.mainloop()