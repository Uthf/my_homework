# этот блок определяет функции, которые можно сделать с числами

def addition(a, b):
    return print("Ответ: {} + {} =".format(a, b), a + b)


def subtraction(a, b):
    return print("Ответ: {} - {} =".format(a, b), a - b)


def multiplication(a, b):
    return print("Ответ: {} * {} =".format(a, b), a * b)


def division(a, b):
    return print("Ответ: {} / {} =".format(a, b), a / b)


def exponentation(a, b):
    return print("Ответ: {} ** {}".format(a, b), a ** b)


first_number = int(input("Введите первое число:\n" ))
operation = int(input("Введи операцию:\n"))
second_number = int(input("Введите второе число:\n"))

# этот блок выводит решение
if operation == 1:
    solution = addition(first_number, second_number)
elif operation == 2:
    solution = subtraction(first_number, second_number)
elif operation == 3:
    solution = multiplication(first_number, second_number)
elif operation == 4:
    solution = division(first_number,second_number)
elif operation == 5:
    solution = exponentation(first_number, second_number)
else:
    print("Попробуй начать сначала.")
    