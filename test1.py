from collections import Counter


a = list(map(int, input().split()))
print(max(dict.values(Counter(a))))
